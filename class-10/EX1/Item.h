#pragma once
#include<iostream>
#include<string>
#include<algorithm>


class Item
{
public:
	Item(std::string name, std::string serialNumber, double unitPrice);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.
	friend std::ostream& operator<<(std::ostream& os, const Item& other);


	//get and set functions
	double get_unitPrice() const;
	int get_count() const;
	std::string get_serialNumber() const;
	std::string get_name() const;

	void set_unitPrice(double unitPrice);
	void set_count(int count);
	void set_serialNumber(std::string serialNumber);
	void set_name(std::string name);

private:
	std::string _name;
	std::string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};