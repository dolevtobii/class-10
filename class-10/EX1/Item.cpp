#include "Item.h"

Item::Item(std::string name, std::string serialNumber, double unitPrice)
{
	this->_name = name;
	this->_serialNumber = serialNumber;
	this->_unitPrice = unitPrice;
	this->_count = 1;
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return this->_count * this->_unitPrice;
}

bool Item::operator<(const Item& other) const
{
	bool result = false;

	if(this->_serialNumber < other._serialNumber)
	{
		result = true;
	}

	return result;
}

bool Item::operator>(const Item& other) const
{
	bool result = false;

	if (this->_serialNumber > other._serialNumber)
	{
		result = true;
	}

	return result;
}

bool Item::operator==(const Item& other) const
{
	bool result = false;

	if (this->_serialNumber == other._serialNumber)
	{
		result = true;
	}

	return result;
}

std::ostream& operator<<(std::ostream& os, const Item& other)
{
	os << "      price: " << other.get_unitPrice() << "   " << other.get_serialNumber() << ". " << other.get_name()<< std::endl;
	return os;
}

double Item::get_unitPrice() const
{
	return this->_unitPrice;
}

int Item::get_count() const
{
	return this->_count;
}

std::string Item::get_serialNumber() const
{
	return this->_serialNumber;
}

std::string Item::get_name() const
{
	return this->_name;
}

void Item::set_unitPrice(double unitPrice)
{
	this->_unitPrice = unitPrice;
}

void Item::set_count(int count)
{
	this->_count = count;
}

void Item::set_serialNumber(std::string serialNumber)
{
	this->_serialNumber = serialNumber;
}

void Item::set_name(std::string name)
{
	this->_name = name;
}
