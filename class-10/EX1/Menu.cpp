#include "Menu.h"

void option1(std::map<std::string, Customer> &abcCustomers, Item itemList[], string newCustomerName)
{
	int i = 0;
	int	userChoiceProduct = 0;

	do
	{
		cout << "The items you can buy are : (0 to exit)" << endl;
		for (i = 0; i < 10; i++)
		{
			cout << itemList[i];
		}
		cout << "What item would you like to buy?" << endl;
		cin >> userChoiceProduct;

		if (userChoiceProduct >= 1 && userChoiceProduct <= 10)
		{
			abcCustomers.insert(std::pair<string, Customer>(newCustomerName, Customer(newCustomerName)));
			abcCustomers[newCustomerName].addItem(itemList[userChoiceProduct - 1]);
		}
	} while (userChoiceProduct != 0);
	
}

void option2(std::map<std::string, Customer>& abcCustomers, Item itemList[], string newCustomerName)
{
	int i = 0;
	int	userChoiceProduct = 0;

	do
	{
		cout << "What item would you like to remove?" << endl;
		cin >> userChoiceProduct;

		if (userChoiceProduct >= 1 && userChoiceProduct <= 10)
		{
			abcCustomers[newCustomerName].removeItem(itemList[userChoiceProduct - 1]);
		}
	} while (userChoiceProduct != 0);
}
