#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(std::string name);
	Customer();
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item product);//add item to the set
	void removeItem(Item product);//remove item from the set

	//get and set functions
	std::string get_name() const;
	std::set<Item> get_items() const;

	void set_name(std::string name);

	void print_items();

private:
	std::string _name;
	std::set<Item> _items;
	
};