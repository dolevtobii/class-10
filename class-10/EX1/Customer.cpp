#include "Customer.h"

Customer::Customer(std::string name)
{
	this->_name = name;
}

Customer::Customer()
{
}

Customer::~Customer()
{
}

double Customer::totalSum() const
{
	double result = 0;
	std::set<Item>::iterator it;
	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		result += it->totalPrice();
	}

	return result;
}

void Customer::addItem(Item product)
{
	std::set<Item>::iterator it = this->_items.find(product);
	

	if(it == this->_items.end())
	{
		this->_items.insert(product);
	}
	else
	{
		Item tempItem(it->get_name(), it->get_serialNumber(), it->get_unitPrice());
		tempItem.set_count(it->get_count() + 1);
		this->_items.erase(*it);
		this->_items.insert(tempItem);
	}
}

void Customer::removeItem(Item product)
{
	std::set<Item>::iterator it = this->_items.find(product);
	

	if(it != this->_items.end())
	{
		if(it->get_count() == 1)
		{
			this->_items.erase(*it);
		}
		else
		{
			Item tempItem(it->get_name(), it->get_serialNumber(), it->get_unitPrice());
			tempItem.set_count(it->get_count() - 1);
			this->_items.erase(*it);
			this->_items.insert(tempItem);
		}
	}
}

std::string Customer::get_name() const
{
	return this->_name;
}

std::set<Item> Customer::get_items() const
{
	return this->_items;
}

void Customer::set_name(std::string name)
{
	this->_name = name;
}

void Customer::print_items()
{
	std::set<Item>::iterator it2;

	for (it2 = this->_items.begin(); it2 != this->_items.end(); it2++)
	{
		std::cout << "count: " << it2->get_count() << *it2;
	}
}
