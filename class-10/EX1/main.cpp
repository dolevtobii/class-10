#include "Customer.h"
#include "Menu.h"
#include<map>

using std::cout;
using std::cin;
using std::endl;
using std::string;

int main()
{
	int i = 0;
	int	userChoice = 0;
	int	userChoiceProduct = 0;
	int	userChoiceAction = 0;

	string newCustomerName = "";

	std::map<std::string, Customer>::iterator it_map;
	std::map<std::string, Customer>::iterator it_map_bigger;
	std::set<Item>::iterator it_set;

	std::map<std::string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","1",5.3),
		Item("Cookies","2",12.6),
		Item("bread","3",8.9),
		Item("chocolate","4",7.0),
		Item("cheese","5",15.3),
		Item("rice","6",6.2),
		Item("fish", "8", 31.65),
		Item("chicken","7",25.99),
		Item("cucumber","9",1.21),
		Item("tomato","10",2.32) };

	cout << "Welcome to MagshiMart!" << endl;
	do
	{
		cout << "\n1. to sign as customer and buy items \n2. to uptade existing customer's items \n3. to print the customer who pays the most \n4. to exit" << endl;
		cin >> userChoice;

		switch(userChoice)
		{
		case 1:
			cout << "Enter new customer name: ";
			cin >> newCustomerName;
			it_map = abcCustomers.find(newCustomerName);
			if(it_map == abcCustomers.end())
			{
				option1(abcCustomers, itemList, newCustomerName);
			}
			else
			{
				cout << "User already exist! try again" << endl;
			}
			break;

		case 2:
			cout << "Enter customer name: ";
			cin >> newCustomerName;
			it_map = abcCustomers.find(newCustomerName);
			if (it_map == abcCustomers.end())
			{
				cout << "User NOT exist! try again" << endl;
			}
			else
			{
				do
				{
					it_map->second.print_items();
					cout << "\n1. Add items \n2. Remove items \n3. Back to menu" << endl;
					cout << "Enter your choice: ";
					cin >> userChoiceAction;

					switch (userChoiceAction)
					{
					case 1:
						option1(abcCustomers, itemList, newCustomerName);
						break;

					case 2:
						option2(abcCustomers, itemList, newCustomerName);
						break;

					case 3:
						break;

					default:
						cout << "Wrong number! try again!";

					}

				} while (userChoiceAction != 3);

			}
			break;

		case 3:
			it_map_bigger = abcCustomers.begin();
			for (it_map = abcCustomers.begin(); it_map != abcCustomers.end(); ++it_map)
			{
				if (it_map->second.totalSum() > it_map_bigger->second.totalSum())
				{
					it_map_bigger = it_map;
				}
			}

			cout << "Name: " << it_map_bigger->second.get_name() << "      Total payment: " << it_map_bigger->second.totalSum() << endl;

			break;

		case 4:
			cout << "GoodBye! :)" << endl;
			break;

		default:
			cout << "Wrong number! try again!" << endl;
		}

	} while (userChoice != 4);


	return 0;
}
